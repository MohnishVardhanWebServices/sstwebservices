
Pod::Spec.new do |s|

  s.name         = "SSTWebServices"
  s.version      = "0.0.9"
  s.summary      = "A short description of SSTWebServices."
  s.homepage     = "https://MohnishVardhanWebServices@bitbucket.org/MohnishVardhanWebServices/sstwebservices.git"
  s.license      = "MIT"
  s.author             = { "mohnish" => "vmohnish@thisisswitch.com" }
   s.platform     = :ios, "10.0"
  s.source       = { :git => "https://MohnishVardhanWebServices@bitbucket.org/MohnishVardhanWebServices/sstwebservices.git", :tag => s.version.to_s }
  s.source_files  = "SSTWebServices", "SSTWebServices/**/*.{h,m}"
end
