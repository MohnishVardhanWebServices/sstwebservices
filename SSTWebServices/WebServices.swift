//
//  WebServices.swift
//  CheersAndBeers
//
//  Created by Uma B on 5/11/17.
//  Copyright © 2017 UMA B. All rights reserved.
//

import Foundation
import UIKit
public enum MethodType {
    case GET,POST,PUT,DELETE
    
   fileprivate func getMethodName() -> String {
        switch self {
        case .GET:
            return "GET"
        case .POST:
            return "POST"
        case .PUT:
            return "PUT"
        case .DELETE:
            return "DELETE"
        }
    }
}
public typealias successBlock = (_ result:Any) -> Void
public typealias failureBlock = (_ errMsg:String) -> Void

public class WebServices: NSObject {

    
    public class func parseData(urlStr:String,parameters:Any?,method:MethodType,flg:NSInteger ,successHandler:@escaping successBlock,failureHandler:@escaping failureBlock){
        
        let url = URL(string: urlStr.replacingOccurrences(of: " ", with: "%20"))
        var request = URLRequest(url: url!)
        request.cachePolicy = .reloadIgnoringCacheData
        print(url ?? "")
        request.httpMethod = method.getMethodName()
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if(parameters != nil){
            request.httpBody = self.convertParametersToData(parms: parameters! as AnyObject) as Data?
        }
        self.callWebservice(request: request) { (result, errMsg) in
            if errMsg == nil {
                successHandler(result as Any)
            }else{
                failureHandler(errMsg! as String)
            }
        }
    }
    private
    class func callWebservice(request:URLRequest,completionBlock:@escaping (Any?,String?) -> () ){
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, err) in
            if err == nil {
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    DispatchQueue.main.async {
                        completionBlock(result,nil)
                    }
                }
                catch let JSONError as NSError{
                    completionBlock(nil,JSONError.localizedDescription)
                }
                
            }else{
                completionBlock(nil,err?.localizedDescription)
            }
            
            }.resume()
    }
    private
    class func convertParametersToData(parms:AnyObject) -> NSData?{
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonStr = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
            return jsonStr!.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false) as NSData?
        }catch let err as NSError{
            print("JSON ERROR\(err.localizedDescription)")
        }
        catch {
            // Catch any other errors
        }
        return nil
    }
    
    //Uploading an image
    
   public class func uploadFile(_ urlStr:String,img:UIImage,successHandler:@escaping successBlock,failureHandler:@escaping failureBlock){
        //  self.showProgressiveHud(true, loadingTxt: "")
    
        let url = URL(string:urlStr)
        var request = URLRequest(url:url!)
        request.httpMethod = "POST"
        let boundary = "Boundary-\(UUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let image_data = UIImageJPEGRepresentation(img,0.5)
        let body = NSMutableData()
        
        let fname = "businesspics.jpg"
        let mimetype = "image/jpg"
        
        //define the data post parameter
        
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append(image_data!)
        body.append("\r\n".data(using: String.Encoding.utf8)!)
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        
        request.httpBody = body as Data
        
        self.callWebservice(request: request) { (result, errMsg) in
            if errMsg == nil {
                successHandler(result as Any)
            }else{
                failureHandler(errMsg! as String)
            }
        }
    }
//    fileprivate func checkReachbily()->Bool{
//        switch reachability.currentReachabilityStatus {
//        case .notReachable:
//            return false
//        case .reachableViaWiFi:
//            return true
//        case .reachableViaWWAN:
//            return true
//        }
//        return false
//    }
   fileprivate class func showProgressiveHud (showHud : NSInteger){
//        DispatchQueue.main.async {
//            
//            var progressHUD = MBProgressHUD()
//            if (showHud >= 1){
//                var ishudloading = false
//                for vw in (appDel.window?.subviews)!{
//                    if vw.isKind(of: (MBProgressHUD.self)){
//                        DispatchQueue.main.async {
//                            ishudloading = true
//                        }
//                    }
//                }
//                if (ishudloading == false){
//                    progressHUD = MBProgressHUD.showAdded(to: appDel.window!, animated: true)
//                    progressHUD.label.text = "Requesting..."
//                }
//            }
//            else{
//                for vw in (appDel.window?.subviews)!{
//                    if vw.isKind(of: (MBProgressHUD.self)){
//                        DispatchQueue.main.async {
//                            vw.removeFromSuperview()
//                            return
//                        }
//                    }
//                }
//            }
//        }
    }
}

